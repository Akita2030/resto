# Resto

Author: Borkoev(Akita2030) Aktan
Packages: (
        Python,
        SQLite,
        Django,
        Django rest,
        HTML,
        CSS
            )

How to run:
    Step1: Run virtual environment,

    Step2: Change directory path resto/,

    Step3: Type 'python manage.py runserver'


How to install dependencies:

    pip install -r requirements.txt
                or
    pip3 install -r requirements.txt


Steps of execution:
    Main App
    URLs:
        http://<servername>/registration/ -> Registration page
        http://<servername>/login/ -> Login by username page
        http://<servername>/logout/ -> Logout page
        http://<servername>/main/ -> Main page
        http://<servername>/about/ -> About page keeps information about our company
        http://<servername>/feedbacks/ -> Users feedbacks for dish
        http://<servername>/feedback/create/ -> Creating feedback page
        http://<servername>/feedback/detail/<int:pk>/ -> Feedback details page
        http://<servername>/feedback/update/<int:pk>/ -> Feedback update page
        http://<servername>/feedback/delete/<int:pk>/ -> Feedback delete page
    APIs:
        http://<servername>/api/users/ -> Users list page
        http://<servername>/api/user-profiles/ -> User profiles list page
        http://<servername>/api/feedbacks/ -> Feedbacks list page
        http://<servername>/api/comments/ -> Comments list page
        http://<servername>/api/comment/create/ -> Comments creating page
        http://<servername>/api/comment/detail/<int:pk>/ -> Feedbacks comment detail
        http://<servername>/api/feedback/create/ -> Feedback creating page
        http://<servername>/api/user/<int:pk>/feedbacks/ -> Users feedbacks list page
        http://<servername>/api/user/<int:pk>/comments/ -> Users comments list page 
        http://<servername>/api/feedback/delete/<int:pk>/ -> Feedback deleting page
        http://<servername>/api/comment/delete/<int:pk>/ -> Comment deleting page
    Menu App
    URLs:
        http://<servername>/meals/ -> Page with all meals 
        http://<servername>/wines/ -> Page with all wines
    APIs:
        http://<servername>/api/meals/ -> Page with all meals
        http://<servername>/api/wines/ -> Page with all wines
        http://<servername>/api/primary-meals/ -> Page with all primary meals
        http://<servername>/api/rates/ -> Page with all rates
        http://<servername>/api/meal/primary/ -> Creating primary meal page
        http://<servername>/api/meal/create/ -> Meal creating page
        http://<servername>/api/rate/create/ -> Rate creating page
        http://<servername>/api/wine/create/ -> Wine creating page
        http://<servername>/api/meal/delete/<int:pk>/ -> Meal deleting page
        http://<servername>/api/rate/delete/<int:pk>/ -> Rate deleting page
        http://<servername>/api/primary-meal/delete/<int:pk>/ -> Primary Meal deleting page
        http://<servername>/api/wine/delete/<int:pk>/ -> Wine deleting page
    Reservation App
    URLs:
        http://<servername>/reservation/ -> Table reservation pages
        http://<servername>/orders/ -> Reservation list page
    APIs:
        http://<servername>/api/reservations/ -> Reservation list page
        http://<servername>/api/reservation/create/ -> Reservation creating page
        http://<servername>/api/user/<int:pk>/reservations/ -> All users reservations 
        http://<servername>/api/reservation/delete/<int:pk>/ -> Reservation deleting page
    Teams App
    URLs:
        http://<servername>/team/ -> Cooks list page 
        http://<servername>/team/details/<int:pk>/ -> Cook details page
    APIs:
        http://<servername>/api/cooks/ -> Cooks list page
        http://<servername>/api/cook/create/ -> Cook creating page
        http://<servername>/api/cook/delete/<int:pk>/ -> Cook deleting page