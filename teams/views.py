from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404, render, redirect
from django.views.generic import ListView, CreateView
from django.views.generic.base import ContextMixin
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from main.models import UserProfile
from teams.forms import CookForm

from teams.models import Cook
from django.views import View

####################################################################################################
####################################################################################################
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.generics import CreateAPIView, DestroyAPIView, UpdateAPIView

from.serializers import CookListSerializer
####################################################################################################

class CookView(View):
    def get(self,request):
        username = Cook.objects.all()
        form = CookForm()
        return render(request=request,
            template_name = 'teams/team.html',
            context={'form':form,'cook_list':username}
        )
    def post(self, request):
        form = CookForm(request.POST)
        profile_id = request.POST.get('cook_source')
        cook = Cook.objects.filter(cook_source = profile_id)
        username = Cook.objects.all()
        if cook:
            return render(request=request,template_name='teams/team.html',context={'error':'Такой повар уже есть','cook_list':username})
        if form.is_valid():
            cook_form = form.save(commit=False)
            user_form = User.objects.get(pk=request.user.pk)
            profile_values = UserProfile.objects.get(user = user_form)
            cook_form.cook_source = profile_values
            cook_form.save()
            return redirect('teams:cook-list')
        else:
            form = CookForm()
            return render(request=request,template_name='teams/team.html',context={'form':form,'cook_list':username})
    


class CookDetailView(DetailView, LoginRequiredMixin):
    model = Cook
    template_name = 'teams/teams_details.html'
   

####################################################################################################
####################################################################################################
####################################################################################################


class CookListAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_classes = CookListSerializer

    def get(self, request):
        cooks = Cook.objects.all()
        serializer = self.serializer_classes(cooks, many=True)

        return Response(
            status=status.HTTP_200_OK,
            data=serializer.data
        )

class CookAPICreateView(CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = CookListSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            status=status.HTTP_201_CREATED,
            data={
                'success': True,
                'message': "Cook has been created successfuly!",
                'result': serializer.data,
            }
        )


class CookDestroyAPIView(DestroyAPIView):
    permission_class = (AllowAny,)
    queryset = Cook.objects.all()
    model = Cook