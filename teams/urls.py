from django.urls import path

from .views import (CookAPICreateView, 
CookView, 
CookDetailView, 
CookListAPIView,
CookDestroyAPIView,
)

app_name = 'teams'

urlpatterns = [
    path('team/', CookView.as_view(), name='cook-list'),
    path('team/details/<int:pk>/', CookDetailView.as_view(), name='cook-detail'),

####################################################################################################

    path('api/cooks/',CookListAPIView.as_view(),name='api-cooks'),
    path('api/cook/create/',CookAPICreateView.as_view(), name='api-cook-create'),
    path('api/cook/delete/<int:pk>/', CookDestroyAPIView.as_view(), name='api-delete-cook'),
]