from rest_framework.serializers import ModelSerializer

from teams.models import Cook

class CookListSerializer(ModelSerializer):
    class Meta:
        model = Cook
        fields = '__all__'