from django.db import models
from django.contrib.auth.models import User

from main.models import UserProfile


COOK_EDUCATION = (
    ('University degree','University degree'),
    ('Specialized secondary education','Specialized secondary education'),
    ('Secondary education','Secondary education'),
    ('Without education','Without education')
)

class Cook(models.Model):
    cook_source = models.OneToOneField(
        to=UserProfile,
        on_delete=models.CASCADE,
    )

    work_experience = models.IntegerField()

    pass_work_place = models.TextField()

    education = models.CharField(
        choices=COOK_EDUCATION,
        default='University degree',
        max_length=40
    )

    def __str__(self) :
        return self.education