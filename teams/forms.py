from django import forms
from django.forms.models import ModelForm

from .models import Cook

class CookForm(ModelForm):
    class Meta:
        model = Cook
        fields = ('cook_source',
                    'work_experience',
                    'pass_work_place',
                    'education',
                   'education',)