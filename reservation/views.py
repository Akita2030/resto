from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.shortcuts import render, redirect, reverse
from django.views.generic import ListView, CreateView
from reservation.forms import ReservationForm

from reservation.models import Reservation
from main.models import UserProfile

####################################################################################################
####################################################################################################
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.generics import CreateAPIView, DestroyAPIView, UpdateAPIView

from .serializers import UserListSerializer

from.serializers import ReservationListSerializer, UserProfileListSerializer
####################################################################################################

class ReservationCreateView(LoginRequiredMixin, CreateView):
    model = Reservation
    template_name = 'reservation/reservation.html'
    

    def post(self, request):
        book_form = ReservationForm(request.POST)
        if book_form.is_valid():
            form = book_form.save(commit=False)
            user_form = User.objects.get(pk=request.user.pk)
            profile_values = UserProfile.objects.get(user = user_form)
            form.user = profile_values
            form.save()
            return redirect('reservation:orders')
        else:
            book_form = ReservationForm()

        return render(
            request=request,
            template_name='reservation/reservation.html',
            context={"form": book_form}
    )
    form_class = ReservationForm


class ReservationListView(LoginRequiredMixin,ListView):
    model = Reservation
    template_name = 'reservation/orders.html'
   

####################################################################################################
####################################################################################################
####################################################################################################

class ReservationListAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_classes = ReservationListSerializer

    def get(self, request):
        reservations = Reservation.objects.all()
        serializer = self.serializer_classes(reservations, many=True)

        return Response(
            status=status.HTTP_200_OK,
            data=serializer.data
        )

class ReservationAPICreateView(CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = ReservationListSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            status=status.HTTP_201_CREATED,
            data={
                'success': True,
                'message': "Reservation has been created successfuly!",
                'result': serializer.data,
            }
        )

class UserReservationListAPIView(APIView):
    permission_class = (AllowAny,)
    serializer_class = UserListSerializer

    def get(self, request, pk):
        user = User.objects.filter(pk=pk).first()

        if not user:
            return Response(
                status=status.HTTP_404_NOT_FOUND,
                data={
                    'success': False,
                    'result': 'User does not exist!'
                }
            )

        serializer = self.serializer_class(user)

        return Response(
            status=status.HTTP_200_OK,
            data={
                'success': True,
                'result': serializer.data
            }
        )

class ReservationDestroyAPIView(DestroyAPIView):
    permission_class = (AllowAny,)
    queryset = Reservation.objects.all()
    model = Reservation