from rest_framework.serializers import ModelSerializer

from reservation.models import Reservation

from main.models import User, UserProfile 

class ReservationListSerializer(ModelSerializer):
    class Meta:
        model = Reservation
        fields = '__all__'


class UserProfileListSerializer(ModelSerializer):
    users = ReservationListSerializer(required = False, many = True)
    
    class Meta:
        model = UserProfile
        fields = (
            'age',
            'profile_photo',
            'favorite_anime',
            'users',
            )

class UserListSerializer(ModelSerializer):
    profile = UserProfileListSerializer(required=False)
    class Meta:
        model = User
        fields = (
            'is_superuser',
            'username',
            'first_name',
            'last_name',
            'email',
            'profile',
        )