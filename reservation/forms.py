from django import forms
from django.forms.models import ModelForm

from reservation.models import Reservation

class ReservationForm(ModelForm):
    class Meta:
        model = Reservation
        exclude = ['date_created_at','user']
        widgets = {
                'message': forms.Textarea(
                    attrs={
                        'cols': 35, 
                        'rows': 3
                    }
                ),
                'date': forms.DateInput(
                    attrs={
                        'class': 'form-control datetimepicker-input',
                        'data-target': '#datetimepicker4',
                        'placeholder':'Date',
                    }
                ),
                'time': forms.TimeInput(
                    attrs=
                    {
                        'class': 'form-control timepicker-input',
                        'data-target': '#datetimepicker3',
                        'placeholder':'Time',
                        'type': 'time'
                    }
                ),
                'amount_of_users': forms.Select(
                    attrs={
                        'class':'form-control',
                        'id':'selectPerson'
                    }
                )
        }