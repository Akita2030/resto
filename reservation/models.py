from django.db import models
from django.db.models.deletion import CASCADE
from django.utils import timezone
from django.contrib.auth.models import User
from django.shortcuts import reverse

from main.models import UserProfile

AMOUNT_CHOICES=(
    ('1','1'),
    ('2','2'),
    ('3','3'),
    ('4','4'),
    ('5','5'),
    ('6','6')
)

class Reservation(models.Model):
    user = models.ForeignKey(
        to=UserProfile,
        on_delete=CASCADE,
        related_name='users'
    )

    phone_number = models.CharField(max_length=50)

    amount_of_users = models.CharField(
        choices=AMOUNT_CHOICES,
        max_length=15
        )

    date = models.DateField()

    time = models.TimeField()

    message = models.TextField()

    date_created_at = models.DateTimeField(default=timezone.now,editable=False)


    def get_absolute_url(self):
        return reverse("reservation:orders")
















