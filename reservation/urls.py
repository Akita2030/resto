from django.urls import path
from .views import (ReservationAPICreateView, 
ReservationListView, 
ReservationCreateView, 
ReservationListAPIView, 
UserReservationListAPIView,
ReservationDestroyAPIView,
)

app_name = 'reservation'

urlpatterns = [
    path('reservation/',ReservationCreateView.as_view(),name='reservations'),
    path('orders/', ReservationListView.as_view(), name='orders'),

####################################################################################################

    path('api/reservations/',ReservationListAPIView.as_view(),name='api-reservations'),
    path('api/reservation/create/',ReservationAPICreateView.as_view(), name='api-reservation-create'),
    path('api/user/<int:pk>/reservations/', UserReservationListAPIView.as_view(), name='api-users-reservations'),
    path('api/reservation/delete/<int:pk>/', ReservationDestroyAPIView.as_view(), name='api-delete-reservation'),
]
