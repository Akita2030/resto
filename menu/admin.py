from django.contrib import admin
from .models import Rate, Meal, PrimaryMeal, Wine

admin.site.register(Rate)
admin.site.register(PrimaryMeal)
admin.site.register(Meal)
admin.site.register(Wine)
