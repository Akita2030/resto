from datetime import datetime
from django.db import models
from django.db.models.fields import DateTimeField
from django.utils import timezone

CUISINE_CHOICES = (
    ('Итальянская','Итальянская'),
    ('Французская','Французская'),
    ('Узбекская','Узбекская'),
    ('Русская','Русская'),
    ('Киргизская','Киргизская'),
)
WINE_CATEGORIES = (
        ('Сладкое', 'Сладкое'),
        ('Полу-Сладкое', 'Полу-Сладкое'),
        ('Cухое', 'Cухое'),
        ('Полу-Сухое', 'Полу-Сухое'),
    )
WINE_COLORS = (
        ('Белое', 'Белое'),
        ('Красное', 'Красное'),
        ('Розовое', 'Розовое')
    )
WINE_PRIORITY = (
    ('Лучшее Вино','Лучшее Вино'),
    ('Обычное Вино','Обычное Вино'),
)
CATEGORY_CHOICES = (
    ('Breakfast','Breakfast'),
    ('Lunch','Lunch'),
    ('Dinner','Dinner')
)

class Rate(models.Model):
    rating = models.SmallIntegerField()
    date_created_at = models.DateTimeField(default=timezone.now,editable=False)
     
    def __str__(self) -> str:
        return '%s' % self.rating

class PrimaryMeal(models.Model):
    main_dish = models.CharField(max_length=40)
    cuisine = models.CharField(max_length=40,choices=CUISINE_CHOICES,default='Итальянская')
    meal_photo = models.ImageField(upload_to = 'portfolio/')
    ingridients = models.CharField(max_length=128)
    rate = models.ManyToManyField(to=Rate, related_name='rates')
    meal_price = models.SmallIntegerField()
    due_to = models.DateField()
    date_created_at = models.DateTimeField(default=timezone.now,editable=False)

    def __str__(self) -> str:
        return '%s' % self.main_dish

class Meal(models.Model):
    name = models.CharField(max_length=50)
    photo = models.ImageField(upload_to = 'portfolio/')
    price = models.SmallIntegerField()
    meal_ingridients = models.CharField(max_length=128)
    date_created_at = models.DateTimeField(default=timezone.now,editable=False)
    category = models.CharField(max_length=40,choices=CATEGORY_CHOICES, default='Breakfast')

    def __str__(self) -> str:
        return '%s' % self.name

class Wine(models.Model):
    wine_name = models.CharField(max_length=50)
    wine_photo = models.ImageField(upload_to = 'portfolio/')
    description = models.TextField()
    category = models.CharField(max_length=84,choices=WINE_CATEGORIES, default='Сладкое') 
    priority = models.CharField(max_length=32, choices=WINE_PRIORITY, default='Лучшее Вино')
    color = models.CharField(max_length=32, choices=WINE_COLORS, default='Белое')
    wine_rate = models.ManyToManyField(to=Rate, related_name='wine_rates')
    wine_price = models.SmallIntegerField()
    date_created_at = models.DateTimeField(default=timezone.now,editable=False)

    def __str__(self) -> str:
        return '%s' % self.wine_name