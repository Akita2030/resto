from django.shortcuts import render
from django.views.generic.base import View
from django.views.generic import ListView
from datetime import datetime, date
from datetime import timedelta
from django.shortcuts import get_object_or_404

from menu.models import Meal, PrimaryMeal, Rate, Wine

####################################################################################################
####################################################################################################
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.generics import CreateAPIView, DestroyAPIView, UpdateAPIView

from.serializers import WineListSerializer, PrimaryMealListSerializer, MealListSerializer, RateListSerializer
####################################################################################################

class MenuListView(ListView):
        
    def get(self, request):
        breakfasts = Meal.objects.filter(category='Breakfast')
        lunchs = Meal.objects.filter(category='Lunch')
        dinners = Meal.objects.filter(category='Dinner')
        current_date = datetime.now()
        next_year = current_date + timedelta(days=365)
        primary_meals = PrimaryMeal.objects.filter(due_to__range=[current_date, next_year])
        return render(request=request,
            template_name='menu/primary_meals.html',
            context={'primary_meals':primary_meals,
            'breakfasts':breakfasts,'lunchs':lunchs,'dinners':dinners})

    
class WineListView(ListView):
        
    def get(self, request):
        best_priority = Wine.objects.filter(priority='Лучшее Вино')
        common_priority = Wine.objects.filter(priority='Обычное Вино')
        return render(request=request,
            template_name='menu/wines.html',
            context={
            'first_priority':best_priority,'second_priority':common_priority})


####################################################################################################
####################################################################################################
####################################################################################################


class WineListAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_classes = WineListSerializer

    def get(self, request):
        wines = Wine.objects.all()
        serializer = self.serializer_classes(wines, many=True)

        return Response(
            status=status.HTTP_200_OK,
            data=serializer.data
        )

class RateListAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_classes = RateListSerializer

    def get(self, request):
        rates = Rate.objects.all()
        serializer = self.serializer_classes(rates, many=True)

        return Response(
            status=status.HTTP_200_OK,
            data=serializer.data
        )

class PrimaryMealListAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_classes = PrimaryMealListSerializer

    def get(self, request):
        primary_meals = PrimaryMeal.objects.all()
        serializer = self.serializer_classes(primary_meals, many=True)

        return Response(
            status=status.HTTP_200_OK,
            data=serializer.data
        )

class MealListAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_classes = MealListSerializer

    def get(self, request):
        meals = Meal.objects.all()
        serializer = self.serializer_classes(meals, many=True)

        return Response(
            status=status.HTTP_200_OK,
            data=serializer.data
        )

class PrimaryMealAPICreateView(CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = PrimaryMealListSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            status=status.HTTP_201_CREATED,
            data={
                'success': True,
                'message': "Primary Meal has been created successfuly!",
                'result': serializer.data,
            }
        )

class MealAPICreateView(CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = MealListSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            status=status.HTTP_201_CREATED,
            data={
                'success': True,
                'message': "Meal has been created successfuly!",
                'result': serializer.data,
            }
        )

class RateAPICreateView(CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = RateListSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            status=status.HTTP_201_CREATED,
            data={
                'success': True,
                'message': "Rate has been created successfuly!",
                'result': serializer.data,
            }
        )

class WineAPICreateView(CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = WineListSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            status=status.HTTP_201_CREATED,
            data={
                'success': True,
                'message': "Wine has been created successfuly!",
                'result': serializer.data,
            }
        )

class WineDestroyAPIView(DestroyAPIView):
    permission_class = (AllowAny,)
    queryset = Wine.objects.all()
    model = Wine

class PrimaryMealDestroyAPIView(DestroyAPIView):
    permission_class = (AllowAny,)
    queryset = PrimaryMeal.objects.all()
    model = PrimaryMeal

class MealDestroyAPIView(DestroyAPIView):
    permission_class = (AllowAny,)
    queryset = Meal.objects.all()
    model = Meal

class RateDestroyAPIView(DestroyAPIView):
    permission_class = (AllowAny,)
    queryset = Rate.objects.all()
    model = Rate