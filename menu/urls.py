from django.urls import path
from .views import (MenuListView, 
WineListView, 
WineListAPIView, 
PrimaryMealListAPIView, 
MealListAPIView, 
RateListAPIView, 
PrimaryMealAPICreateView, 
MealAPICreateView,
WineAPICreateView,
RateAPICreateView,
MealDestroyAPIView,
PrimaryMealDestroyAPIView,
WineDestroyAPIView,
RateDestroyAPIView
)


app_name = 'menu'

urlpatterns = [
    path('meals/',MenuListView.as_view(),name='meals'),
    path('wines/', WineListView.as_view(),name='wines'),

####################################################################################################

    path('api/wines/',WineListAPIView.as_view(),name='api-wines'),
    path('api/meals/',MealListAPIView.as_view(),name='api-meals'),
    path('api/primary-meals/',PrimaryMealListAPIView.as_view(),name='api-primary-meals'),
    path('api/rates/',RateListAPIView.as_view(),name='api-rates'),
    path('api/meal/primary/', PrimaryMealAPICreateView.as_view(), name='create-primary-meal'),
    path('api/meal/create/', MealAPICreateView.as_view(), name='create-meal'),
    path('api/rate/create/', RateAPICreateView.as_view(), name='create-rate'),
    path('api/wine/create/', WineAPICreateView.as_view(), name='create-wine'),
    path('api/rate/delete/<int:pk>/', RateDestroyAPIView.as_view(), name='api-delete-comment'),
    path('api/wine/delete/<int:pk>/', WineDestroyAPIView.as_view(), name='api-delete-comment'),
    path('api/primary-meal/delete/<int:pk>/', PrimaryMealDestroyAPIView.as_view(), name='api-delete-comment'),
    path('api/meal/delete/<int:pk>/', MealDestroyAPIView.as_view(), name='api-delete-meal'),
]
