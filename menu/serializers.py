from rest_framework.serializers import ModelSerializer

from menu.models import Rate, Wine, PrimaryMeal, Meal 

class RateListSerializer(ModelSerializer):
    class Meta:
        model = Rate
        fields = '__all__' 

class WineListSerializer(ModelSerializer):
    def create(self, validated_data):
        wine_rate = validated_data.pop('wine_rate', [])
        wine =  Wine(**validated_data)
        wine.save()

        wine.wine_rate.set(wine_rate)

        return wine
        
    class Meta:
        model = Wine
        fields = '__all__'


class PrimaryMealListSerializer(ModelSerializer):
    def create(self, validated_data):
        rate = validated_data.pop('rate', [])
        primary_meal =  PrimaryMeal(**validated_data)
        primary_meal.save()

        primary_meal.rate.set(rate)

        return primary_meal

    class Meta:
        model = PrimaryMeal
        fields = '__all__'

class MealListSerializer(ModelSerializer):
    class Meta:
        model = Meal
        fields = '__all__'