from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse

class UserProfile(models.Model):
    user = models.OneToOneField(
        to=User,
        on_delete=models.CASCADE,
        related_name='profile',
    )

    profile_photo = models.ImageField(upload_to = 'users/profiles/', default = 'default.png')

    age = models.SmallIntegerField(default=0)

    favorite_anime = models.CharField(max_length=25,default='Unknown')

    def __str__(self):
        return '%s %s' % (self.user.first_name, self.user.last_name)


class Feedback(models.Model):
    author = models.ForeignKey(to=UserProfile,on_delete=models.CASCADE,related_name='authors')
    feedback_text = models.TextField()
    date_created = models.DateTimeField(default=timezone.now,editable=False)

    def get_absolute_url(self):
        return reverse("main:feedback-details", kwargs={"pk": self.pk})

    def __str__(self):
        return '%s' % self.feedback_text

class Comment(models.Model):
    author = models.ForeignKey(to=UserProfile, on_delete=models.CASCADE, related_name='comment_authors')
    assigned_to_feedback = models.ForeignKey(to=Feedback, on_delete=models.CASCADE, related_name='assigned_feedbacks')
    comment_text = models.TextField()
    date_created = models.DateTimeField(default=timezone.now,editable=False)

    def get_absolute_url(self):
        return reverse("main:feedback-details", kwargs={"pk": self.pk})

    def __str__(self):
        return '%s' % self.comment_text