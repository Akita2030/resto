from http.client import responses
from django.contrib.auth.models import User
from django.http import request
from django.shortcuts import render, redirect, reverse
from django.views import View
from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.views import LoginView, LogoutView
from rest_framework import permissions, serializers

from main.models import Feedback, Comment
from.forms import CustomUserCreationForm, FeedbackCreateForm, CommentForm
from .models import UserProfile
from django.contrib.auth import authenticate, login

####################################################################################################
####################################################################################################
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.generics import CreateAPIView, DestroyAPIView, UpdateAPIView
from rest_framework.authentication import BasicAuthentication


from.serializers import UserCommentsSerializer, UserListSerializer,UserProfileListSerializer, FeedbackListSerializer, CommentListSerializer, UserFeedbacksSerializer
####################################################################################################

class MainView(View):
    def get(self, request):
        return render(
            request=request,
            template_name='main/main.html'
        )

class AboutView(View):
    def get(self, request):
        return render(
            request=request,
            template_name='main/about.html'
        )

class RegistrationRestoView(View):
    
    def get(self, request):
        user_creation_form = CustomUserCreationForm()
        return render(
            request=request,
            template_name='main/registration.html',
            context={'form':user_creation_form}
        )
    def post(self, request):
        user_creation_form = CustomUserCreationForm(request.POST)
        
        if user_creation_form.is_valid():
            user_creation_form.save()
            return redirect('login')

        return redirect('registration')


class FeedbackListView(ListView):
    model = Feedback
    template_name = 'main/feedback_list.html'
    context_object_name = 'feedback_list'

class FeedbackCreateView(LoginRequiredMixin,CreateView):
    model = Feedback
    template_name = 'main/feedback_create.html'
    form_class = FeedbackCreateForm


    def post(self, request):
        feedback_form = self.form_class(request.POST)
        if feedback_form.is_valid():
            form = feedback_form.save(commit=False)
            user_obj = User.objects.get(pk=request.user.pk)
            profile_values = UserProfile.objects.get(user = user_obj)
            form.author = profile_values
            form.save()
            return redirect('main:feedback-list')
        feedback_form = self.form_class()

        return render(
            request=request,
            template_name='main/feedback_create.html',
            context={"form": feedback_form})
        
class FeedbackDetailsView(DetailView):
    model = Feedback
    template_name = 'main/feedback_details.html'

    def get_context_data(self, **kwargs):
        comments = Comment.objects.all()
        comment_form = CommentForm()
        kwargs['form'] = comment_form
        kwargs['comments'] = comments
        return super().get_context_data(**kwargs)

    def post(self, request, pk):
        comment_form = CommentForm(request.POST)
        comments = Comment.objects.all()
        author = User.objects.get(pk=request.user.pk)
        feedback = self.get_object()

        if comment_form.is_valid():
            form = comment_form.save(commit=False)
            form.author = author
            form.assigned_to_feedback = feedback
            form.save()

        comment_form = CommentForm()

        return render(
            request=request,
            template_name='main/feedback_details.html',
            context={'form': comment_form, 'title': 'Details of Feedbacks', 'comments': comments, 'object':feedback}
        )
        
    

    
class FeedbackUpdateView(LoginRequiredMixin,UserPassesTestMixin,UpdateView):
    model = Feedback
    template_name = 'main/feedback_update.html'
    fields = 'feedback_text',

    def test_func(self):
        feedback = self.get_object()
        user_obj = User.objects.get(pk=self.request.user.pk)
        user = UserProfile.objects.get(user = user_obj)

        if feedback.author == user:
            return True
        return False

class FeedbackDeleteView(LoginRequiredMixin,UserPassesTestMixin,DeleteView):
    model = Feedback
    template_name = 'main/feedback_delete.html'

    def test_func(self):
        feedback = self.get_object()
        user_obj = User.objects.get(pk=self.request.user.pk)
        user = UserProfile.objects.get(user = user_obj)

        if feedback.author == user:
            return True
        return False
    
    def get_success_url(self):
        return reverse('main:feedback-list')


####################################################################################################
####################################################################################################
####################################################################################################

class UserListAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_classes = UserListSerializer
    authentication_classes = (BasicAuthentication, )

    def get(self, request):
        users = User.objects.all()
        serializer = self.serializer_classes(users, many=True)

        return Response(
            status=status.HTTP_200_OK,
            data=serializer.data
        )

class UserProfileListAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_classes = UserProfileListSerializer

    def get(self, request):
        user_profiles = UserProfile.objects.all()
        serializer = self.serializer_classes(user_profiles, many=True)

        return Response(
            status=status.HTTP_200_OK,
            data=serializer.data
        )

class CommentListAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_classes = CommentListSerializer

    def get(self, request):
        comments = Comment.objects.all()
        serializer = self.serializer_classes(comments, many=True)

        return Response(
            status=status.HTTP_200_OK,
            data=serializer.data
        )

class FeedbackListAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_classes = FeedbackListSerializer

    def get(self, request):
        feedbacks = Feedback.objects.all()
        serializer = self.serializer_classes(feedbacks, many=True)

        return Response(
            status=status.HTTP_200_OK,
            data=serializer.data
        )

class LoginAPIView(APIView):
    def post(self, request):
        username = request.POST.get('username')
        password = request.POST.get('password')

        if not username or not password:
            return Response(
                status=status.HTTP_401_UNAUTHORIZED,
                data={
                    'success': False,
                    'result': "Username or Password wasn't provided"
                })
        user = authenticate(request=request ,username=username, password=password)

        if user:
            login(request,user)
            return Response(
                status=status.HTTP_200_OK,
                data={
                    'success': True,
                    'result': "You have been authorized successfuly!",
                }
            )
        return Response(
                status=status.HTTP_401_UNAUTHORIZED,
                data={
                    'success': False,
                    'result': "Wrong credentials!",
                }
            )

class UserProfileAPICreateView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserProfileListSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            status=status.HTTP_201_CREATED,
            data={
                'success': True,
                'message': "UserProfile has been created successfuly!",
                'result': serializer.data,
            }
        )

class UserProfileAPICreateView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserProfileListSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            status=status.HTTP_201_CREATED,
            data={
                'success': True,
                'message': "UserProfile has been created successfuly!",
                'result': serializer.data,
            }
        )

class FeedbackAPICreateView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = FeedbackListSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            status=status.HTTP_201_CREATED,
            data={
                'success': True,
                'message': "Feedback has been created successfuly!",
                'result': serializer.data,
            }
        )

class CommentCreateAPIView(APIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = CommentListSerializer


    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(
            status = status.HTTP_201_CREATED,
            data={
                'success':True,
                'result':serializer.data,
            }
            )

class CommentAPIView(APIView):
    permission_class = (IsAuthenticated,)
    serializer_class = CommentListSerializer

    def get(self, request, pk):
        all_comments = Comment.objects.filter(assigned_to_feedback_id = pk)

        serializer = self.serializer_class(all_comments, many=True)

        return Response(
            status=status.HTTP_200_OK,
            data={
                'success': True,
                'result': serializer.data})
           

class UserFeedbacksListAPIView(APIView):
    permission_class = (IsAuthenticated,)
    serializer_class = UserFeedbacksSerializer
    authentication_classes = (BasicAuthentication,)

    def get(self, request, pk):
        user = User.objects.filter(pk=pk).first()

        if not user:
            return Response(
                status=status.HTTP_404_NOT_FOUND,
                data={
                    'success': False,
                    'result': 'User does not exist!'
                }
            )

        serializer = self.serializer_class(user)

        return Response(
            status=status.HTTP_200_OK,
            data={
                'success': True,
                'result': serializer.data
            }
        )

class UserCommentsListAPIView(APIView):
    permission_class = (IsAuthenticated,)
    serializer_class = UserCommentsSerializer
    authentication_classes = (BasicAuthentication,)

    def get(self, request, pk):
        user = User.objects.filter(pk=pk).first()

        if not user:
            return Response(
                status=status.HTTP_404_NOT_FOUND,
                data={
                    'success': False,
                    'result': 'User does not exist!'
                }
            )

        serializer = self.serializer_class(user)

        return Response(
            status=status.HTTP_200_OK,
            data={
                'success': True,
                'result': serializer.data
            }
        )

class FeedbackDestroyAPIView(DestroyAPIView):
    permission_class = (IsAuthenticated,)
    queryset = Feedback.objects.all()
    model = Feedback

class CommentDestroyAPIView(DestroyAPIView):
    permission_class = (IsAuthenticated,)
    queryset = Comment.objects.all()
    model = Comment



