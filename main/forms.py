from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Feedback, Comment
from django.forms import ModelForm

class CustomUserCreationForm(UserCreationForm):
    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)
    email = forms.CharField(max_length=50)
    fields = (
        'first_name',
        'last_name',
        'email'
    )

class FeedbackCreateForm(ModelForm):
    class Meta:
        model = Feedback
        exclude = ['author', 'date_created']

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = 'comment_text',