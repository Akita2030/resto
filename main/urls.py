from os import name
from django.urls import path
from .views import (MainView, 
AboutView, 
FeedbackCreateView, 
FeedbackListView, 
FeedbackDetailsView, 
FeedbackUpdateView, 
FeedbackDeleteView, 
UserListAPIView, 
UserProfileListAPIView, 
FeedbackListAPIView, 
CommentListAPIView, 
CommentCreateAPIView, 
CommentAPIView, 
FeedbackAPICreateView, 
UserFeedbacksListAPIView, 
UserCommentsListAPIView, 
FeedbackDestroyAPIView,
CommentDestroyAPIView)

app_name = 'main'

urlpatterns = [
    path('main/', MainView.as_view(), name='main'),
    path('about/', AboutView.as_view(), name='about'),
    path('feedbacks/', FeedbackListView.as_view(),name='feedback-list'),
    path('feedback/create/', FeedbackCreateView.as_view(),name='feedback-create'),
    path('feedback/detail/<int:pk>/', FeedbackDetailsView.as_view(),name='feedback-details'),
    path('feedback/update/<int:pk>/', FeedbackUpdateView.as_view(),name='feedback-update'),
    path('feedback/delete/<int:pk>/', FeedbackDeleteView.as_view(),name='feedback-delete'),

    ####################################################################################################
    path('api/users/', UserListAPIView.as_view(), name='api-users-list'),
    path('api/user-profiles/', UserProfileListAPIView.as_view(), name='api-user-profiles-list'),
    path('api/feedbacks/', FeedbackListAPIView.as_view(), name='api-feedbacks-list'),
    path('api/comments/', CommentListAPIView.as_view(), name='api-comment-list'),
    path('api/comment/create/', CommentCreateAPIView.as_view(), name='api-comment-create'),
    path('api/comment/detail/<int:pk>/', CommentAPIView.as_view(), name='api-comment-detail'),
    path('api/feedback/create/', FeedbackAPICreateView.as_view(),name='api-feedback-create'),
    path('api/user/<int:pk>/feedbacks/', UserFeedbacksListAPIView.as_view(), name='api-users-feedback'),
    path('api/user/<int:pk>/comments/', UserCommentsListAPIView.as_view(), name='api-users-comment'),
    path('api/feedback/delete/<int:pk>/', FeedbackDestroyAPIView.as_view(), name='api-delete-feedback'),
    path('api/comment/delete/<int:pk>/', FeedbackDestroyAPIView.as_view(), name='api-delete-comment'),
]
