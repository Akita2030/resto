from django.db.models import signals
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver

from django.contrib.auth.models import User
from .models import UserProfile

@receiver(signal=post_save, sender=User)
def user_profile_create(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)

@receiver(signal=post_save, sender=User)
def user_profile_save(sender, instance, **kwargs):
    instance.profile.save()