from rest_framework.serializers import ModelSerializer, SlugRelatedField
from django.contrib.auth.models import User

from main.models import Comment, Feedback, UserProfile

class UserProfileListSerializer(ModelSerializer):
    # authors = FeedbackListSerializer(required = False, many = True)
    # comment_authors = CommentListSerializer(required = False, many =True)
    class Meta:
        model = UserProfile
        fields = (
            'age',
            'profile_photo'
            ,'favorite_anime',
            # 'authors',
            # 'comment_authors',
            )

class UserListSerializer(ModelSerializer):
    profile = UserProfileListSerializer(required=False)
    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
            'email',
            'profile',
        )


class CommentListSerializer(ModelSerializer):
    def create(self, validated_data):
        feedback_comment = Comment(**validated_data)
        feedback_comment.save()

        return feedback_comment

    class Meta:
        model = Comment
        fields = '__all__'
        read_only_fields = ('date_created',)


class UserCommentsSerializer(ModelSerializer):
    profile = UserProfileListSerializer(required=False)
    
    def to_representation(self, instance):
        representation = super().to_representation(instance)
        comments = CommentListSerializer(instance.profile.comment_authors.all(), many=True)
        representation["comments"] = comments.data

        return representation

    class Meta:
        model = User
        fields = (
                'first_name',
                'last_name',
                'email',
                'profile',
            )


class FeedbackListSerializer(ModelSerializer):
    class Meta:
        model = Feedback
        fields = '__all__'


class UserFeedbacksSerializer(ModelSerializer):
    profile = UserProfileListSerializer(required=False)
    
    def to_representation(self, instance):
        representation = super().to_representation(instance)
        feedbacks = FeedbackListSerializer(instance.profile.authors.all(), many=True)
        representation["feedbacks"] = feedbacks.data

        return representation

    class Meta:
        model = User
        fields = (
                'first_name',
                'last_name',
                'email',
                'profile',
            )









