from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from main import views as main_views
from main.views import LogoutView, MainView, LoginView, RegistrationRestoView, LoginAPIView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('main.urls')),
    path('registration/', RegistrationRestoView.as_view(), name='registration'),
    path('login/', LoginView.as_view(template_name = 'main/login.html'), name='login'),
    path('logout/', LogoutView.as_view(template_name='main/logout.html'), name='logout'),
    path('',include('teams.urls')),
    path('',include('reservation.urls')),
    path('',include('menu.urls')),

####################################################################################################

    path('api-auth/', include('rest_framework.urls')),
    path('api/login/',LoginAPIView.as_view(), name='api-login'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
